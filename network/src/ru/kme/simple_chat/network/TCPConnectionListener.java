package ru.kme.simple_chat.network;

import java.io.IOException;

public interface TCPConnectionListener {
    void onConnectionReady(TCPConnection tpcConnection);

    void onReceiveString(TCPConnection tcpConnection, String s);

    void onDisconnect(TCPConnection tcpConnection);

    void onException(TCPConnection tcpConnection, IOException e);
}
